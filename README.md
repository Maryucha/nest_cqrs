
<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

# Tarefa Desafio API Rest NestJs - Parte 3 CQRS

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)


### Dependencies

- @nestjs/common : ^8.0.0
- @nestjs/core : ^8.0.0
- @nestjs/cqrs : ^8.0.3
- @nestjs/platform-express : ^8.0.0
- @nestjs/typeorm : ^8.0.3
- class-transformer : ^0.5.1
- class-validator : ^0.13.2
- pg : ^8.7.3
- reflect-metadata : ^0.1.13
- rimraf : ^3.0.2
- rxjs : ^7.2.0
---
## Table of contents

- [Tarefa Desafio API Rest NestJs - Parte 3 CQRS](#tarefa-desafio-api-rest-nestjs---parte-3-cqrs)
    - [Dependencies](#dependencies)
  - [- rxjs : ^7.2.0](#--rxjs--720)
  - [Table of contents](#table-of-contents)
  - [Definition of done](#definition-of-done)
    - [Entrega](#entrega)
  - [Comandos utilizados em sequência](#comandos-utilizados-em-sequência)
  - [Lições aprendidas](#lições-aprendidas)
      - [Novos comandos no nest cli](#novos-comandos-no-nest-cli)
  - [Running the app](#running-the-app)
  - [Code scaffolding](#code-scaffolding)
  - [Documentation](#documentation)
  - [* SWAGGER link](#-swagger-link)
  - [Build](#build)
  - [Test](#test)
    - [unit tests](#unit-tests)
    - [test coverage](#test-coverage)

---
## Definition of done

Maryucha elaborar uma api rest com NEST usando o Desing Patern CQRS
EXTRAS:  

- [ ] Testes

### Entrega

Essa solução foi entregue com:

- [ ] Testes
- [x] Compodoc
- [x] Docker
- [x] Lint-Staged
- [x] Documentação Swagger
- [x] Banco de Dados
- [x] Documentação Técnica
- [x] Camada de validação de Dados

 ---

## Comandos utilizados em sequência

```bash
# Instalar globalemnte o CLI
$ npm i @nestjs/cli

# Criar projeto novo
$ nest new nest-cqrs

# Entrar na pasta do projeto
$ cd nest-cqrs

# Iniciar o projeto com comando --watch para recompilar
$ npm run start:dev

# Comando que gera toda estrutura de um entity
$ nest g resource

# Código de criação da documentação técnica
$ npm run compodoc

# Instalacao do PG
$ npm install pg --save

# Instalação do swagger
$ npm install --save @nestjs/swagger swagger swagger-ui-express class-validator
```
---
## Lições aprendidas
Durante a codificação dessa solução facilmente pude observar que a arquitetura proposta trás grandes ganhos na parte de refatoração de código, e compreesão para quem não domina tanto assim a organização do projeto.

#### Novos comandos no nest cli

  > esse é o comando do cli do nest para criar arquivos setando a flag de remoção dos arquivis de spec.

  ```bash
    nest g cl person/commands/impl/update-person.command --no-spec
  ```

---
## Running the app

```bash
npm run start:dev
```
---
## Code scaffolding

Run `nest generate|g <schematic> <name> [options]` to generate a new Nest Element.

---

---
## Documentation

> Gerar a documentação técnica automáticamente na pasta 
* /documentation
* 
```bash
npm run compodoc
```
> Documentação da API via SwaggerUi
  * [SWAGGER link](http://localhost:3004/swagger/)
---

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

---

## Test

### unit tests

```bash
npm run test
```
### test coverage

```bash
npm run test:cov
```

---

Nest is [MIT licensed](LICENSE).

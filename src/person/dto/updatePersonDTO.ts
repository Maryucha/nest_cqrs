/**
 * This Interface represents on DTO for transaction objects
 */
export interface UpdatePersonDto {
    name?: string;
    age?: number;
}
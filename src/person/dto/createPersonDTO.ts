/**
 * This Interface represents on DTO for transaction objects
 */
export interface CreatePersonDto {
    name: string;
    age: number;
}
import { IQueryHandler, QueryHandler } from "@nestjs/cqrs";
import { InjectRepository } from "@nestjs/typeorm";
import { Person } from "src/person/entities/person.entity";
import { PersonRepository } from "src/person/person.repository";
import { GetPersonsQuery } from "./get-persons.query";

/**
 * This class represents the logic of the persons selection query
 */
@QueryHandler(GetPersonsQuery)
export class GetPersonsHandler implements IQueryHandler<GetPersonsHandler> {

    constructor(
        @InjectRepository(PersonRepository) private personRepo: PersonRepository
    ) {}

    /**
     * This method executes the logic by passing the query
     * @param query 
     */
    async execute(query: GetPersonsHandler): Promise<Person[]> {
        try {
            return await this.personRepo.find();
        } catch (error) {
            console.log(error);
        }
    }
    
}

import { GetPersonByIdQuery } from './get-person.query';
import { IQueryHandler, QueryHandler } from "@nestjs/cqrs";
import { InjectRepository } from '@nestjs/typeorm';
import { PersonRepository } from 'src/person/person.repository';
import { Person } from 'src/person/entities/person.entity';

/**
 * This class represents the logic of the person selection query by id
 */
@QueryHandler(GetPersonByIdQuery)
export class GetPersonHandler implements IQueryHandler<GetPersonByIdQuery> {

    constructor(
        @InjectRepository(PersonRepository) private personRepo: PersonRepository
    ) { }


    /**
     * This method executes the logic by passing the query
     * @param query 
     */
    async execute(query: GetPersonByIdQuery): Promise<Person> {
        return await this.personRepo.findOneOrFail(query.id);
    }

}

/**
 * This class represents a query
 */
export class GetPersonByIdQuery {
    constructor(public id: string){ }
}

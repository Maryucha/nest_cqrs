import { UpdatePersonHandler } from './commands/handler/update-person/update-person.handler';
import { DeletePersonHandler } from './commands/handler/delete-person/delete-person.handler';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PersonController } from './person.controller';
import { PersonRepository } from './person.repository';
import { CreatePersonHandler } from './commands/handler/create-person/create-person.handler';
import { GetPersonHandler } from './queries/handlers/lists-person/get-person.handler';
import { CreatePersonValidation } from './commands/handler/create-person/create-person.validator';
import { UpdatePersonValidation } from './commands/handler/update-person/update-person.validator';
import { GetPersonsHandler } from './queries/handlers/lists-person/get-persons.handler';

@Module({
  imports: [TypeOrmModule.forFeature([PersonRepository]), CqrsModule],
  controllers: [PersonController],
  providers: [GetPersonsHandler, CreatePersonHandler, GetPersonHandler, DeletePersonHandler,CreatePersonValidation,UpdatePersonValidation,UpdatePersonHandler]
})
export class PersonModule { }

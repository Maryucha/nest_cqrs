import { UpdatePersonDto } from './dto/updatePersonDTO';
import { DeletePersonCommand } from './commands/handler/delete-person/delete-person.command';
import { GetPersonByIdQuery } from './queries/handlers/lists-person/get-person.query';
import { Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreatePersonCommand } from './commands/handler/create-person/create-person.command';
import { GetPersonsQuery } from './queries/handlers/lists-person/get-persons.query';
import { UpdatePersonCommand } from './commands/handler/update-person/update-person.command';

/**
 * This class is responsabilit for list all endpoints for person
 */
@Controller('persons')
@ApiTags('Persons')
export class PersonController {

  constructor(private queryBus: QueryBus, private commandBus: CommandBus) { }

  /**
   * List all people running the query
   * @returns Person[]
   */
  @Get()
  @ApiOperation({ summary: 'Listar todas os Pessoas' })
  async getAll() {
    return await this.queryBus.execute(new GetPersonsQuery());
  }

  /**
  * List one people running the query
  * @returns Person
  */
  @Get(':id')
  @ApiOperation({ summary: 'Listar uma pessoa pelo id' })
  @UsePipes(new ValidationPipe({ transform: true }))
  async getById(@Param('id') id: string) {
    return await this.queryBus.execute(new GetPersonByIdQuery(id));
  }

  /**
   * Create a person by running the create command
   * @returns Person
   */
  @Post()
  @HttpCode(201)
  @ApiOperation({ summary: 'Criar uma Pessoa' })
  @UsePipes(new ValidationPipe({ transform: true }))
  async create(@Body(ValidationPipe) payload: CreatePersonCommand) {
    return await this.commandBus.execute(payload);
  }


  /**
  * Update a person by running the update command
  * @param payload UpdateDTO
  * @param id  string
  */
  @Patch(':id')
  @ApiOperation({ summary: 'Editar uma Pessoa' })
  @UsePipes(new ValidationPipe({ transform: true }))
  async update(@Body(ValidationPipe) payload: UpdatePersonDto, @Param('id') id: string) {
    console.log('payload', payload);

    const command = new UpdatePersonCommand(id, payload);
    console.log('command', command)
    return await this.commandBus.execute(command);
  }

  /**
   * Create a person by running the delete command
   * @param id string
   */
  @Delete(':id')
  @ApiOperation({ summary: 'Deletar uma Pessoa' })
  @UsePipes(new ValidationPipe({ transform: true }))
  async delete(@Param('id') id: string) {
    var command = new DeletePersonCommand(id);
    return await this.commandBus.execute(command);
  }
}

import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
 
/**
 * This class represents Person model
 */
@Entity({name:'persons'})
export class Person extends BaseEntity {

  @PrimaryGeneratedColumn('uuid')
  id?: string;
 
  @ApiProperty({
    name: 'This is name person',
    type: String,
  })
  @Column({name:'name', type: 'varchar', length: 200})
  name: string;
 
  @ApiProperty({
    name: 'This is age person',
    type: Number,
  })
  @Column({name:'age', type: 'numeric'})
  age: number;
}




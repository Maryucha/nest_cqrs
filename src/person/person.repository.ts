import { EntityRepository, Repository } from "typeorm";
import { CreatePersonDto } from "./dto/createPersonDTO";
import { Person } from "./entities/person.entity";

/**
 * This class represents the persistence layer with the database following Person.entity
 */
@EntityRepository(Person)
export class PersonRepository extends Repository<Person> {
 }

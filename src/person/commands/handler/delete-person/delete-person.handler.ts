import { DeletePersonCommand } from './delete-person.command';
import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { InjectRepository } from '@nestjs/typeorm';
import { PersonRepository } from 'src/person/person.repository';

/**
 * 
This class is responsible for the deletion logic
 */
@CommandHandler(DeletePersonCommand)
export class DeletePersonHandler implements ICommandHandler<DeletePersonCommand>{
    constructor(
        @InjectRepository(PersonRepository) private personRepo: PersonRepository
    ) { }

    /**
     * Method that applies the command to delete the repository
     * @param command 
     */
   async execute(command: DeletePersonCommand) {
        return await this.personRepo.delete(command.id);
    }
}

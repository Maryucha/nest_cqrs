/**
 * This class represents the payload in the form of command
 */
export class DeletePersonCommand {
    constructor(public id: string) { }
}

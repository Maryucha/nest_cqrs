import { Injectable, UnprocessableEntityException } from "@nestjs/common";
import { UpdatePersonCommand } from "./update-person.command";

/**
 * This class represents a validation layer
 */
@Injectable()
export class UpdatePersonValidation {
  validate(command: UpdatePersonCommand): void {

    if (command.payload.name.length > 0 && command.payload.name.length < 3) {
      throw new UnprocessableEntityException(
        'Name must contain at least three letters.',
      );
    }

    if (command.payload.age > 0 && command.payload.age < 18) {
      throw new UnprocessableEntityException(
        'Age must 18 age old.',
      );
    }
  }
}
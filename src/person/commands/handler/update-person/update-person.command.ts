import { UpdatePersonDto } from "src/person/dto/updatePersonDTO";

/**
 * This class represents the payload in the form of command
 */
export class UpdatePersonCommand {
    constructor(public id: string, public payload: UpdatePersonDto) { }
}

import { UpdatePersonValidation } from './update-person.validator';
import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { InjectRepository } from '@nestjs/typeorm';
import { PersonRepository } from 'src/person/person.repository';
import { UpdatePersonCommand } from './update-person.command';

/**
 * This class is responsible for update and validating person
 */
@CommandHandler(UpdatePersonCommand)
export class UpdatePersonHandler implements ICommandHandler<UpdatePersonCommand> {

    constructor(
        @InjectRepository(PersonRepository) private personRepo: PersonRepository,
        private validator: UpdatePersonValidation
    ) { }


    /**
     * This method runs the build logic by going through the validation layer
     * @param command 
     */
    async execute(command: UpdatePersonCommand): Promise<any> {
        this.validator.validate(command);

        let person = await this.personRepo.findOne(command.id);

        person.name = command.payload.name;
        person.age = command.payload.age;

        await this.personRepo.save(person);
    }
}

/**
 * This class represents the payload in the form of command
 */
export class CreatePersonCommand {
    name: string;
    age: number;
}

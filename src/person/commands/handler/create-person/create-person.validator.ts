import { Injectable, UnprocessableEntityException } from "@nestjs/common";
import { CreatePersonCommand } from "./create-person.command";

/**
 * This class represents a validation layer
 */
@Injectable()
export class CreatePersonValidation {
  validate(command: CreatePersonCommand): void {

    if (!command.name || !command.age) {
      throw new UnprocessableEntityException();
    }

    if (command.name.length < 3) {
      throw new UnprocessableEntityException(
        'Name must contain at least three letters.',
      );
    }

    if (command.age < 18) {
      throw new UnprocessableEntityException(
        'Age must 18 age old.',
      );
    }
  }
}
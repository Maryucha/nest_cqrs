import { PersonRepository } from '../../../person.repository';
import { CreatePersonCommand } from './create-person.command';
import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { InjectRepository } from '@nestjs/typeorm';
import { Person } from 'src/person/entities/person.entity';
import { CreatePersonValidation } from './create-person.validator';

/**
 * This class is responsible for creating and validating person
 */
@CommandHandler(CreatePersonCommand)
export class CreatePersonHandler implements ICommandHandler<CreatePersonCommand> {

    constructor(
        @InjectRepository(PersonRepository) private personRepo: PersonRepository,
        private validator : CreatePersonValidation 
    ) { }

    /**
     * This method runs the build logic by going through the validation layer
     * @param command 
     */
    async execute(command: CreatePersonCommand): Promise<void> {
        this.validator.validate(command);
            var newPerson = new Person();
            newPerson.name = command.name;
            newPerson.age = command.age;
            
            await this.personRepo.save(newPerson);
    }
}
